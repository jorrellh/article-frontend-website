import React from 'react';
import { render } from "react-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { 
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Expenses from './routes/expenses';
import Articles from './routes/articles';
import Article from './routes/articles/article';

const rootElement = document.getElementById("root");
render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} >
        <Route path="expenses" element={<Expenses />} />
        <Route path="articles" element={<Articles />} >
        <Route 
            index
            element={
              <main style={{padding: "1rem"}}>
                <p>Select an article</p>
              </main>
            }
          />
          <Route path=":articleId" element={<Article />} />
        </Route>
        <Route
              path="*"
              element={
                <main style={{ padding: "1rem" }}>
                  <p>There's nothing here!</p>
                </main>
              }
            />
      </Route>
    </Routes>
  </BrowserRouter>, 
  rootElement
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
