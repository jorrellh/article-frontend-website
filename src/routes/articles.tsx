import React from 'react';
import { Outlet, NavLink } from 'react-router-dom';
import { getArticles } from '../data';

export default function Articles(){
    let articles = getArticles();
    return(
        <div style={{display: "flex"}}>
            <nav 
                style={{
                    borderRight: "solid 1px",
                    padding: "1rem"
                }}
            >
                {articles.map(article => (
                    <NavLink
                        style={({ isActive }) => {
                            return {
                                display: "block", 
                                margin: "1rem 0",
                                color: isActive ? "red" : ""
                            };
                        }}
                        to={`/articles/${article.id}`}
                        key={article.id}
                    >
                        {article.title}
                    </NavLink>
                ))}
            </nav>
            <Outlet />

        </div>
    );
}