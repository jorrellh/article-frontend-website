import { useParams } from "react-router-dom";
import { getArticle } from "../../data";

export default function Article(){
    let params = useParams();
    if (getArticle(parseInt(params.articleId!, 10))) {

        let article = getArticle(parseInt(params.articleId!, 10));

        return (
            <main style={{padding: "1rem"}}>
                <h2>{article?.title}</h2>
                <p>{article?.body}</p>
                <p>User: {article?.user_id}</p>
            </main>
        );
    } else {
        return (
            <h2>Nothing here!</h2>
        );
    }
}