let articles = [
    {
      id: 1,
      title: "Santa Monica",
      body: "Lorem ipsum dolar sit amet",
      user_id: 2
    },
    {
      id: 2,
      title: "Santa Monica",
      body: "Lorem ipsum dolar sit amet",
      user_id: 2
    },

];

export function getArticles(){
    return articles;
}

export function getArticle(number: number){
    return articles.find(article => article.id === number);
}
